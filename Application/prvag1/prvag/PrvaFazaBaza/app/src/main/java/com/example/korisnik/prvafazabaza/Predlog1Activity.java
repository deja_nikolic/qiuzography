package com.example.korisnik.prvafazabaza;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kosalgeek.asynctask.AsyncResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Predlog1Activity extends AppCompatActivity implements AsyncResponse {
    Button potvrdi;
    EditText txt1,edt1,edt2,edt3,edt4,tacno;
    String txt,p1,p2,p3,p4,correct;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_predlog1);
        txt1=(EditText)findViewById(R.id.text1);
        edt1=(EditText)findViewById(R.id.edt1);
        edt2=(EditText)findViewById(R.id.edt2);
        edt3=(EditText)findViewById(R.id.edt3);
        edt4=(EditText)findViewById(R.id.edt4);
        tacno=(EditText)findViewById(R.id.edtT);

        potvrdi=(Button)findViewById(R.id.PredloziPitanje1);
        potvrdi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                potvrdi(v);

            }
        });

    }
    public void potvrdi(View v){
        txt = txt1.getText().toString();
        p1 = edt1.getText().toString();
        p2 = edt2.getText().toString();
        p3 = edt3.getText().toString();
        p4 = edt4.getText().toString();
        correct=tacno.getText().toString();
        BackGround b = new BackGround();
        b.execute(txt, p1,p2,p3,p4,correct);
    }

  /*  @Override
    public void onClick(View v) {

    }*/

    @Override
    public void processFinish(String s) {

    }
    class BackGround extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String text = params[0];
            String p1 = params[1];
            String p2 = params[2];
            String p3 = params[3];
            String p4 = params[4];
            int pc = Integer.parseInt(params[5]);

            String data = "";
            int tmp;

            try {
                URL url = new URL("http://192.168.1.11/Client/predlog1.php");
                String urlParams = "text=" + text + "&get1=" + p1+ "&get2=" + p2+ "&get3=" + p3
                        + "&get4=" + p4+ "&correct=" + pc;
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while ((tmp = is.read()) != -1) {
                    data += (char) tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
        }


        @Override
        protected void onPostExecute(String s) {
            if(s.equals("")){
                s="Data saved successfully.";

            }
            Toast.makeText(Predlog1Activity.this, s, Toast.LENGTH_LONG).show();
            Intent novi=new Intent(Predlog1Activity.this,IgraActivity.class);
            startActivity(novi);

        }

    }

}
