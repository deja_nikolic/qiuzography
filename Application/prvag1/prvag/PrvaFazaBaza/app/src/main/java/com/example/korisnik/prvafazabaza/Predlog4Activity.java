package com.example.korisnik.prvafazabaza;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kosalgeek.asynctask.AsyncResponse;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Predlog4Activity extends AppCompatActivity implements AsyncResponse {
EditText et1,et2,et3;
    String Naziv;
    String lat,lon;
    Button but;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_predlog4);
        et1=(EditText)findViewById(R.id.et1);
        et2=(EditText)findViewById(R.id.et2);
        et3=(EditText)findViewById(R.id.et3);
        but=(Button)findViewById(R.id.PredloziPitanje4);
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                potvrdi(v);

            }
        });

    }
    public void potvrdi(View v){
        Naziv = et1.getText().toString();
        lat = et2.getText().toString();
        lon =  et3.getText().toString();


        BackGround b = new BackGround();
        b.execute(Naziv,lat,lon);
    }
    @Override
    public void processFinish(String s) {

    }

    class BackGround extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String text = params[0];
            Double p1 = Double.parseDouble(params[1]);
            Double p2 =Double.parseDouble( params[2]);

          //  int pc = Integer.parseInt(params[5]);

            String data = "";
            int tmp;

            try {
                URL url = new URL("http://192.168.1.11/Client/predlog4.php");
                String urlParams = "lokacija=" + text + "&latitude=" + p1+ "&longitude=" + p2;
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while ((tmp = is.read()) != -1) {
                    data += (char) tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if(s.equals("")){
                s="Data saved successfully.";

            }
            Toast.makeText(Predlog4Activity.this, s, Toast.LENGTH_LONG).show();
            Intent novi=new Intent(Predlog4Activity.this,IgraActivity.class);
            startActivity(novi);

        }
    }

}
