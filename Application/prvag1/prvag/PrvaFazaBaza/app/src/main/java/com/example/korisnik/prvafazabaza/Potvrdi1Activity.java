package com.example.korisnik.prvafazabaza;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Potvrdi1Activity extends AppCompatActivity {

    private TextView quizQuestion;
    private Button One;
    private Button Two;
    private Button Three;
    private Button Four;
    private Button Sledece;
    private Button Potvrdi;
    private QuizWrapper firstQuestion;
    private List<QuizWrapper> parsedObject;
    private int quizCount;
    private int currentQuizQuestion;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_potvrdi1);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Potvrdi = (Button) findViewById(R.id.bSubmit);
        One = (Button) findViewById(R.id.b1);
        Two = (Button) findViewById(R.id.b2);
        Three = (Button) findViewById(R.id.b3);
        Four = (Button) findViewById(R.id.b4);
        Sledece = (Button) findViewById(R.id.bNext);
        quizQuestion = (TextView) findViewById(R.id.etQst);
        Potvrdi1Activity.AsyncJsonObject asyncObject = new Potvrdi1Activity.AsyncJsonObject();
        asyncObject.execute("");

        Sledece.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                currentQuizQuestion++;

                if (currentQuizQuestion >= quizCount) {
                    Toast.makeText(Potvrdi1Activity.this, "Nema vise predloga", Toast.LENGTH_LONG).show();
                    Intent novi = new Intent(Potvrdi1Activity.this, AdminActivity.class);
                    startActivity(novi);
                } else {
                    if(quizCount!=0) {
                        firstQuestion = parsedObject.get(currentQuizQuestion);
                        quizQuestion.setText(firstQuestion.getText());
                        One.setText(firstQuestion.getQuestion1());
                        Two.setText(firstQuestion.getQuestion2());
                        Three.setText(firstQuestion.getQuestion3());
                        Four.setText(firstQuestion.getQuestion4());
                    }
                    else{
                        Toast.makeText(Potvrdi1Activity.this, "Nema pitanja za potvrdjivanje", Toast.LENGTH_LONG).show();
                        Intent novi = new Intent(Potvrdi1Activity.this, AdminActivity.class);
                        startActivity(novi);
                    }
                }
            }
        });

        Potvrdi.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v){
                BackGround b = new BackGround();
                String pom= Integer.toString(firstQuestion.getId());
                b.execute(pom);

            }
        });
    }

    private class AsyncJsonObject extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        @Override
        protected String doInBackground(String... params) {
            HttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
            HttpPost httpPost = new HttpPost("http://192.168.1.11/Client/getPotvrdi1.php");
            String jsonResult = "";

            try {
                HttpResponse response = httpClient.execute(httpPost);
                jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
                System.out.println("Returned Json object " + jsonResult.toString());
            } catch (ClientProtocolException e) {
// TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
// TODO Auto-generated catch block
                e.printStackTrace();
            }
            return jsonResult;
        }
        @Override

        protected void onPreExecute() {

// TODO Auto-generated method stub

            super.onPreExecute();

            progressDialog = ProgressDialog.show(Potvrdi1Activity.this, "Downloading Questions","Wait....", true);

        }

        @Override

        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            progressDialog.dismiss();

            if(result.equals("[]")){

                Toast.makeText(Potvrdi1Activity.this, "Nema pitanja za potvrdjivanje", Toast.LENGTH_LONG).show();
                Intent novi = new Intent(Potvrdi1Activity.this, AdminActivity.class);
                startActivity(novi);
                return;
            }


            parsedObject = returnParsedJsonObject(result);

            if(parsedObject == null){

                Toast.makeText(Potvrdi1Activity.this, "Nema pitanja za potvrdjivanje", Toast.LENGTH_LONG).show();
                Intent novi = new Intent(Potvrdi1Activity.this, AdminActivity.class);
                startActivity(novi);


            }

            quizCount=parsedObject.size();
            if(quizCount==0)
            {
                Toast.makeText(Potvrdi1Activity.this, "Nema pitanja za potvrdjivanje", Toast.LENGTH_LONG).show();
                Intent novi = new Intent(Potvrdi1Activity.this, AdminActivity.class);
                startActivity(novi);
            }

            firstQuestion=parsedObject.get(0);

            quizQuestion.setText(firstQuestion.getText());

            //String[] possibleAnswers = firstQuestion.getAnswers().split(",");


            One.setText(firstQuestion.getQuestion1());

            Two.setText(firstQuestion.getQuestion2());

            Three.setText(firstQuestion.getQuestion3());

            Four.setText(firstQuestion.getQuestion4());

        }

        private StringBuilder inputStreamToString(InputStream is) {

            String rLine = "";

            StringBuilder answer = new StringBuilder();

            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            try {

                while ((rLine = br.readLine()) != null) {

                    answer.append(rLine);

                }

            } catch (IOException e) {

// TODO Auto-generated catch block

                e.printStackTrace();

            }

            return answer;

        }

    }
    class BackGround extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            int id = Integer.parseInt(params[0]);

            String data="";
            int tmp;

            try {
                URL url = new URL("http://192.168.1.11/Client/pitanje1potvrdi.php");
                String urlParams = "id="+id;

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();
                InputStream is = httpURLConnection.getInputStream();
                while((tmp=is.read())!=-1){
                    data+= (char)tmp;
                }
                is.close();
                httpURLConnection.disconnect();

                return data;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if(s.equals("")){
                s="Data saved successfully.";

            }
            Toast.makeText(Potvrdi1Activity.this, s, Toast.LENGTH_LONG).show();

        }
    }
    @Override
    public void onBackPressed()
    {

    }
    private List<QuizWrapper> returnParsedJsonObject(String result){
        List<QuizWrapper> jsonObject = new ArrayList<QuizWrapper>();
        JSONObject resultObject = null;
        JSONArray jsonArray = null;
        QuizWrapper newItemObject = null;
        try {
            resultObject = new JSONObject(result);
            System.out.println("Testing the water " + resultObject.toString());
            jsonArray = resultObject.optJSONArray("questions_1");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < jsonArray.length(); i++){
            JSONObject jsonChildNode = null;

            try {
                jsonChildNode = jsonArray.getJSONObject(i);
                int id = jsonChildNode.getInt("id_question");
                String question = jsonChildNode.getString("text");
                String answerOption1 = jsonChildNode.getString("get1");
                String answerOption2 = jsonChildNode.getString("get2");
                String answerOption3 = jsonChildNode.getString("get3");
                String answerOption4 = jsonChildNode.getString("get4");
                int correctAnswer = jsonChildNode.getInt("correct");
                int flagAns=jsonChildNode.getInt("flag");
                newItemObject = new QuizWrapper(id, question, answerOption1, answerOption2, answerOption3, answerOption4, flagAns, correctAnswer);
                jsonObject.add(newItemObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonObject;
    }
}
