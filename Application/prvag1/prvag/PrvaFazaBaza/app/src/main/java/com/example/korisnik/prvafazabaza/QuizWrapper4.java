package com.example.korisnik.prvafazabaza;

public class QuizWrapper4 {
    private int id_question;
    private String text,answ;
    private double lat,lon;

    public QuizWrapper4(int id_question, double la,double lo,String a){
        this.id_question=id_question;
        //this.text=text;
        this.lat=la;
        this.lon=lo;
        this.answ=a;
    }

    public QuizWrapper4(int id_question, String text, double la,double lo){
        this.id_question=id_question;
        this.text=text;
        this.lat=la;
        this.lon=lo;
    }

    public int getId() {
        return id_question;
    }

    public void setId(int id) {

        this.id_question = id_question;

    }

    public String getText() {

        return text;

    }

    public void setText(String text) {

        this.text = text;

    }
    public String getAnsw() {

        return answ;

    }

    public void setAnsw(String text) {

        this.answ = text;

    }

    public  double getLat(){
        return lat;
    }

    public void setLat(){
        this.lat=lat;
    }
    public  double getLon(){
        return lon;
    }

    public void setLon(){
        this.lon=lon;
    }



}
