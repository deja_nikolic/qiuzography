package com.example.korisnik.prvafazabaza;

public class QuizWrapper2 {
    private int id_question;
    private String text;
    private int answer;

    public QuizWrapper2(){}

    public QuizWrapper2(int id_question, String text, int answer){
        this.id_question=id_question;
        this.text=text;
        this.answer=answer;
    }

    public int getId() {
        return id_question;
    }

    public void setId(int id) {

        this.id_question = id_question;

    }

    public String getText() {

        return text;

    }

    public void setText(String text) {

        this.text = text;

    }

    public  int getAnswer(){
        return answer;
    }

    public void setAnswer(){
        this.answer=answer;
    }



}
