package com.example.korisnik.prvafazabaza;

import org.w3c.dom.Text;

/**
 * Created by dejana on 22-May-17.
 */

public class QuizWrapper {
    private int id_question;

    private String text;

    private String get1;

    private String get2;

    private String get3;

    private String get4;

    private int flag;

    private int correct;


    public QuizWrapper(){}

    public QuizWrapper(int id_question, String text, String get1, String get2, String get3, String get4, int flag,  int correct) {

        this.id_question = id_question;
        this.text=text;
        this.get1 = get1;
        this.get2 = get2;
        this.get3 = get3;
        this.get4 = get4;
        this.flag = flag;
        this.correct = correct;

    }

    public int getId() {

        return id_question;

    }

    public void setId(int id) {

        this.id_question = id_question;

    }

    public String getText() {

        return text;

    }

    public void setText(String text) {

        this.text = text;

    }
    public String getQuestion1() {

        return get1;

    }

    public void setQuestion1(String get1) {

        this.get1 = get1;

    }

    public String getQuestion2() {

        return get2;

    }

    public void setQuestion2(String get2) {

        this.get2 = get2;

    }

    public String getQuestion3() {

        return get3;

    }

    public void setQuestion3(String get3) {

        this.get3 = get3;

    }

    public String getQuestion4() {

        return get4;

    }

    public void setQuestion4(String get4) {

        this.get4 = get4;

    }

    public int getFlag() {

        return flag;

    }

    public void setFlag(int flag) {

        this.flag = flag;

    }

    public int getCorrectAnswer() {

        return correct;

    }

    public void setCorrect(int correct) {

        this.correct = correct;

    }

}
