package com.example.korisnik.prvafazabaza;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Predlog2Activity extends AppCompatActivity {
    EditText txt2,tacno;
    Button potvrdi;
    String text,odgovor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_predlog2);
        txt2=(EditText)findViewById(R.id.text2);

        tacno=(EditText)findViewById(R.id.edto);

        potvrdi=(Button)findViewById(R.id.PredloziPitanje2);
        potvrdi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                potvrdi(v);

            }
        });
    }
    public void potvrdi(View v){
        text = txt2.getText().toString();
        odgovor = tacno.getText().toString();

        BackGround b = new BackGround();
        b.execute(text,odgovor);
    }

  /*  @Override
    public void onClick(View v) {

    }*/


    class BackGround extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String text = params[0];
            int odgovor = Integer.parseInt(params[1]);


            String data = "";
            int tmp;

            try {
                URL url = new URL("http://192.168.1.11/Client/predlog2.php");
                String urlParams = "text=" + text + "&answer=" + odgovor;
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while ((tmp = is.read()) != -1) {
                    data += (char) tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if(s.equals("")){
                s="Data saved successfully.";

            }
            Toast.makeText(Predlog2Activity.this, s, Toast.LENGTH_LONG).show();
            Intent novi=new Intent(Predlog2Activity.this,IgraActivity.class);
            startActivity(novi);

        }
    }

}
