package com.example.korisnik.prvafazabaza;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import android.support.v7.app.AppCompatActivity;
import java.util.ArrayList;
import java.util.List;

public class Question1Activity extends AppCompatActivity {
    private  RadioGroup radioGroup;
    private TextView quizQuestion;
    private Button optionOne;
    private Button optionTwo;
    private Button optionThree;
    private Button optionFour;
    private int currentQuizQuestion;
    private int quizCount;
    private QuizWrapper firstQuestion;
    private List<QuizWrapper> parsedObject;
   // private Boolean clicked=false;
    //private int pom=1;
    private int answer=8;
     Integer rezultat;
    String rank;
    String username;


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question1);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        radioGroup = (RadioGroup) findViewById(R.id.rg1);
        quizQuestion = (TextView) findViewById(R.id.etQst);
        optionOne = (Button) findViewById(R.id.btn1);
        optionTwo = (Button) findViewById(R.id.btn2);
        optionThree = (Button) findViewById(R.id.btn3);
        optionFour = (Button) findViewById(R.id.btn4);
        Button nextButton = (Button) findViewById(R.id.btnNext);
       String rez=getIntent().getStringExtra("rezultat");
        rank=getIntent().getStringExtra("rank");
        username=getIntent().getStringExtra("username");
        rezultat=Integer.parseInt(rez);
        AsyncJsonObject asyncObject = new AsyncJsonObject();
        asyncObject.execute("");

        nextButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //  int radioSelected = radioGroup.getCheckedRadioButtonId();
                // int answer = getSelectedAnswer();
                // int correctAnswerForQuestion = firstQuestion.getCorrectAnswer();
                // if (answer == correctAnswerForQuestion) { // correct answer
                Button c1 = (Button) findViewById(R.id.btn1);
                Button c2 = (Button) findViewById(R.id.btn2);
                Button c3 = (Button) findViewById(R.id.btn3);
                Button c4 = (Button) findViewById(R.id.btn4);
                c1.setEnabled(true);
                c2.setEnabled(true);
                c3.setEnabled(true);
                c4.setEnabled(true);
                currentQuizQuestion++;

                if (currentQuizQuestion >= quizCount) {
                    //Toast.makeText(Question1Activity.this, "End of the Quiz Questions", Toast.LENGTH_LONG).show();
                    Intent u=new Intent(Question1Activity.this,Question2Activity.class);
                    u.putExtra("rezultat",rezultat.toString());
                    u.putExtra("username",username);
                    u.putExtra("rank",rank);
                    startActivity(u);
                } else {
                    firstQuestion = parsedObject.get(currentQuizQuestion);
                    quizQuestion.setText(firstQuestion.getText());
                    optionOne.setText(firstQuestion.getQuestion1());
                    optionTwo.setText(firstQuestion.getQuestion2());
                    optionThree.setText(firstQuestion.getQuestion3());
                    optionFour.setText(firstQuestion.getQuestion4());
                }
            }
        });

        final View.OnClickListener myHandler = new View.OnClickListener() {
            public void onClick(View v) {
                Button c1 = (Button) findViewById(R.id.btn1);
                Button c2 = (Button) findViewById(R.id.btn2);
                Button c3 = (Button) findViewById(R.id.btn3);
                Button c4 = (Button) findViewById(R.id.btn4);

                switch (v.getId()) {
                    case R.id.btn1: {
                        answer=1;
                        int correctAnswerForQuestion = firstQuestion.getCorrectAnswer();
                        if (answer == correctAnswerForQuestion) {
                            Toast.makeText(Question1Activity.this, "Tacan odgovor.", Toast.LENGTH_LONG).show();
                            rezultat+=10;

                        } else {
                            Toast.makeText(Question1Activity.this, "Pogresan odgovor.", Toast.LENGTH_LONG).show();
                        }
                        c1.setEnabled(false);
                        c2.setEnabled(false);
                        c3.setEnabled(false);
                        c4.setEnabled(false);

                        break;
                    }
                    case R.id.btn2: {
                        answer=2;
                        int correctAnswerForQuestion = firstQuestion.getCorrectAnswer();
                        if (answer == correctAnswerForQuestion) {
                            Toast.makeText(Question1Activity.this, "Tacan odgovor", Toast.LENGTH_LONG).show();
                            rezultat+=10;
                        } else {
                            Toast.makeText(Question1Activity.this, "Pogresan odgovor.", Toast.LENGTH_LONG).show();
                        }
                        c1.setEnabled(false);
                        c2.setEnabled(false);
                        c3.setEnabled(false);
                        c4.setEnabled(false);
                        break;

                    }
                    case R.id.btn3: {
                        answer=3;
                        int correctAnswerForQuestion = firstQuestion.getCorrectAnswer();
                        if (answer == correctAnswerForQuestion) {
                            Toast.makeText(Question1Activity.this, "Tacan odgovor", Toast.LENGTH_LONG).show();
                            rezultat+=10;
                        } else {
                            Toast.makeText(Question1Activity.this, "Pogresan odgovor.", Toast.LENGTH_LONG).show();
                        }
                        c1.setEnabled(false);
                        c2.setEnabled(false);
                        c3.setEnabled(false);
                        c4.setEnabled(false);
                        break;
                    }
                    case R.id.btn4: {
                        answer=4;
                        int correctAnswerForQuestion = firstQuestion.getCorrectAnswer();
                        if (answer == correctAnswerForQuestion) {
                            Toast.makeText(Question1Activity.this, "Tacan odgovor", Toast.LENGTH_LONG).show();
                            rezultat+=10;
                        } else {
                            Toast.makeText(Question1Activity.this, "Pogresan odgovor.", Toast.LENGTH_LONG).show();
                        }
                        c1.setEnabled(false);
                        c2.setEnabled(false);
                        c3.setEnabled(false);
                        c4.setEnabled(false);
                        break;
                    }

                }


            }

        };
        optionOne.setOnClickListener(myHandler);
        optionTwo.setOnClickListener(myHandler);
        optionThree.setOnClickListener(myHandler);
        optionFour.setOnClickListener(myHandler);
    }
    @Override
    public void onBackPressed()
    {

    }
    private class AsyncJsonObject extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        @Override
        protected String doInBackground(String... params) {
            HttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
            HttpPost httpPost = new HttpPost("http://192.168.1.11/Client/getAll.php");
            String jsonResult = "";

            try {
                HttpResponse response = httpClient.execute(httpPost);
                jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
                System.out.println("Returned Json object " + jsonResult.toString());
            } catch (ClientProtocolException e) {
// TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
// TODO Auto-generated catch block
                e.printStackTrace();
            }
            return jsonResult;
        }

        @Override

        protected void onPreExecute() {

// TODO Auto-generated method stub

            super.onPreExecute();

            progressDialog = ProgressDialog.show(Question1Activity.this, "Downloading Quiz","Wait....", true);

        }

        @Override

        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            progressDialog.dismiss();

            System.out.println("Resulted Value: " + result);

            parsedObject = returnParsedJsonObject(result);

            if(parsedObject == null){

                return;

            }

            quizCount=parsedObject.size();

            firstQuestion=parsedObject.get(0);

            quizQuestion.setText(firstQuestion.getText());

            //String[] possibleAnswers = firstQuestion.getAnswers().split(",");


            optionOne.setText(firstQuestion.getQuestion1());

            optionTwo.setText(firstQuestion.getQuestion2());

            optionThree.setText(firstQuestion.getQuestion3());

            optionFour.setText(firstQuestion.getQuestion4());

        }

        private StringBuilder inputStreamToString(InputStream is) {

            String rLine = "";

            StringBuilder answer = new StringBuilder();

            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            try {

                while ((rLine = br.readLine()) != null) {

                    answer.append(rLine);

                }

            } catch (IOException e) {

// TODO Auto-generated catch block

                e.printStackTrace();

            }

            return answer;

        }

    }

    private List<QuizWrapper> returnParsedJsonObject(String result){
        List<QuizWrapper> jsonObject = new ArrayList<QuizWrapper>();
        JSONObject resultObject = null;
        JSONArray jsonArray = null;
        QuizWrapper newItemObject = null;
        try {
            resultObject = new JSONObject(result);
            System.out.println("Testing the water " + resultObject.toString());
            jsonArray = resultObject.optJSONArray("questions_1");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < jsonArray.length(); i++){
            JSONObject jsonChildNode = null;

            try {
                jsonChildNode = jsonArray.getJSONObject(i);
                int id = jsonChildNode.getInt("id_question");
                String question = jsonChildNode.getString("text");
                String answerOption1 = jsonChildNode.getString("get1");
                String answerOption2 = jsonChildNode.getString("get2");
                String answerOption3 = jsonChildNode.getString("get3");
                String answerOption4 = jsonChildNode.getString("get4");
                int correctAnswer = jsonChildNode.getInt("correct");
                int flagAns=jsonChildNode.getInt("flag");
                newItemObject = new QuizWrapper(id, question, answerOption1, answerOption2, answerOption3, answerOption4, flagAns, correctAnswer);
                jsonObject.add(newItemObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonObject;
    }
}