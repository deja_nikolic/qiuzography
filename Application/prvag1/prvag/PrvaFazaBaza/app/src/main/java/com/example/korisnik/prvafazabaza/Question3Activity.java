package com.example.korisnik.prvafazabaza;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Question3Activity extends AppCompatActivity {
    private ImageView im1;
    private TextView question2;
    private Button submit;
    private EditText answer2;
    private int currentQuizQuestion;
    private int quizCount;
    private QuizWrapper3 firstQuestion;
    private QuizWrapper3 firstImage;
    private List<QuizWrapper3> parsedObject;
    private URI url;
    private static final String IMAGE_URL = "url";
    private JSONArray arrayImages = null;
    private int TRACK = 0;
    private String b;
    private String answer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question3);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        im1 = (ImageView) findViewById(R.id.imageView);
        // submit = (Button) findViewById(R.id.btnSubmit);
        answer2 = (EditText) findViewById(R.id.ans);
        question2 = (TextView) findViewById(R.id.qst2);
        final Button nextButton = (Button) findViewById(R.id.btnNext);
        com.example.korisnik.prvafazabaza.Question3Activity.AsyncJsonObject asyncObject = new com.example.korisnik.prvafazabaza.Question3Activity.AsyncJsonObject();
        asyncObject.execute("");
        EditText et = (EditText) findViewById(R.id.ans);
        // im1 = (ImageView) findViewById(R.id.imageView);

        et.setOnEditorActionListener(new com.example.korisnik.prvafazabaza.Question3Activity.onEditorActionListener() {
            // @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_DONE){
                    nextButton.performClick();
                    String check = answer2.getText().toString();
                    String correctAnswerForQuestion = firstQuestion.getAnswer();
                    if (check == correctAnswerForQuestion) {
                        Toast.makeText(Question3Activity.this, "Tacan odgovor.", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(Question3Activity.this, "Netacan odgovor.", Toast.LENGTH_LONG).show();
                    }
                    return true;
                }
                return false;
            }
        });


        nextButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                currentQuizQuestion++;

                if (currentQuizQuestion >= quizCount) {
                    //  Intent u1=new Intent(com.example.korisnik.prvafazabaza.Question2Activity.this,Question3Activity.class);
                    // startActivity(u1);
                } else {
                    firstQuestion = parsedObject.get(currentQuizQuestion);
                    firstImage=parsedObject.get(currentQuizQuestion);
                    //im1 = (ImageView) findViewById(R.id.imageView);

                    Picasso.with(Question3Activity.this)
                            .load(b=firstImage.getImage())
                            .placeholder(R.drawable.placeholder)   // optional
                            .error(R.drawable.error)      // optional
                            // optional
                            .into(im1);
                    question2.setText(firstImage.getText());
                }
                return;
            }
        });

        final View.OnClickListener myHandler = new View.OnClickListener() {
            public void onClick(View v) {
            }

        };
//dodaj za multiplay!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
         /*  nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  //  int check = Integer.parseInt(answer2.getText().toString());
                  //  if (check == firstQuestion.getAnswer()) {
                 //       Toast.makeText(com.example.korisnik.prvafazabaza.Question3Activity.this, "You got the answer correct", Toast.LENGTH_LONG).show();
                  //  } else {
                //        Toast.makeText(com.example.korisnik.prvafazabaza.Question3Activity.this, "You chose the wrong answer", Toast.LENGTH_LONG).show();
                //    }
                    //String answer="s";
                    String check = answer2.getText().toString();
                    String correctAnswerForQuestion = firstQuestion.getAnswer();
                    if (check == correctAnswerForQuestion) {
                        Toast.makeText(Question3Activity.this, "You got the answer correct", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(Question3Activity.this, "You chose the wrong answer", Toast.LENGTH_LONG).show();
                    }
                }

            });*/

    }

    //private

    private class AsyncJsonObject extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            HttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
            HttpPost httpPost = new HttpPost("http://192.168.1.11/Client/getThree");
            String jsonResult = "";

            try {
                HttpResponse response = httpClient.execute(httpPost);
                jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
                System.out.println("Returned Json object " + jsonResult.toString());
            } catch (ClientProtocolException e) {
// TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
// TODO Auto-generated catch block
                e.printStackTrace();
            }
            return jsonResult;
        }

        @Override

        protected void onPreExecute() {
// TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = ProgressDialog.show(com.example.korisnik.prvafazabaza.Question3Activity.this, "Downloading Quiz", "Wait....", true);
        }

        @Override

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            System.out.println("Resulted Value: " + result);
            parsedObject = returnParsedJsonObject(result);
            if (parsedObject == null) {
                return;
            }

            quizCount = parsedObject.size();
            // firstQuestion = parsedObject.get(0);
            firstImage = parsedObject.get(0);
            //im1 = (ImageView) findViewById(R.id.imageView);
            //  try {
            //JSONObject jsonObject = arrayImages.getJSONObject(TRACK);

            //im1.toString();

            Picasso.with(Question3Activity.this)
                    .load(b=firstImage.getImage())
                    .placeholder(R.drawable.placeholder)   // optional
                    .error(R.drawable.error)      // optional
                    // optional
                    .into(im1);
            question2.setText(firstImage.getText());
            // } catch (JSONException e) {
            //  e.printStackTrace();
            //}

            //}
        }

        private StringBuilder inputStreamToString(InputStream is) {
            String rLine = "";
            StringBuilder answer = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            try {
                while ((rLine = br.readLine()) != null) {
                    answer.append(rLine);
                }
            } catch (IOException e) {
// TODO Auto-generated catch block
                e.printStackTrace();
            }
            return answer;
        }
    }
    @Override
    public void onBackPressed()
    {

    }

    private class onEditorActionListener implements TextView.OnEditorActionListener {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                return true;
            }
            return false;
        }
    }

    private List<QuizWrapper3> returnParsedJsonObject(String result) {
        List<QuizWrapper3> jsonObject = new ArrayList<QuizWrapper3>();
        JSONObject resultObject = null;
        JSONArray jsonArray = null;
        QuizWrapper3 newItemObject = null;
        try {
            resultObject = new JSONObject(result);
            System.out.println("Testing the water " + resultObject.toString());
            jsonArray = resultObject.optJSONArray("questions_3");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonChildNode = null;

            try {
                jsonChildNode = jsonArray.getJSONObject(i);
                int id = jsonChildNode.getInt("id_question");
                String t = jsonChildNode.getString("text");
                String question = jsonChildNode.getString("image");
                String answerCorrect = jsonChildNode.getString("answer");
                newItemObject = new QuizWrapper3(id, t, question, answerCorrect);
                jsonObject.add(newItemObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonObject;
    }

    private void getImage(String urlToImage) {
        class GetImage extends AsyncTask<String, Void, Bitmap> {
            ProgressDialog loading;

            @Override
            protected Bitmap doInBackground(String... params) {
                URL url = null;
                Bitmap image = null;

                String urlToImage = params[0];
                try {
                    url = new URL(urlToImage);
                    image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return image;
            }

        }
    }
}

