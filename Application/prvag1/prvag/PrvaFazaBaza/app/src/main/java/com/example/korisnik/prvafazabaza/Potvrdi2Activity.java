package com.example.korisnik.prvafazabaza;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Potvrdi2Activity extends AppCompatActivity {

    private TextView quizQuestion;
    private Button One;
    private Button Sledece;
    private Button Potvrdi;
    private QuizWrapper2 firstQuestion;
    private List<QuizWrapper2> parsedObject;
    private int quizCount;
    private int currentQuizQuestion;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_potvrdi2);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Potvrdi = (Button) findViewById(R.id.btSubmit);
        One = (Button) findViewById(R.id.bt1);
        Sledece = (Button) findViewById(R.id.btNext);
        quizQuestion = (TextView) findViewById(R.id.Qst);
        Potvrdi2Activity.AsyncJsonObject asyncObject = new Potvrdi2Activity.AsyncJsonObject();
        asyncObject.execute("");

        Sledece.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                currentQuizQuestion++;

                if (currentQuizQuestion >= quizCount) {
                    Toast.makeText(Potvrdi2Activity.this, "Nema vise predloga", Toast.LENGTH_LONG).show();
                    Intent novi = new Intent(Potvrdi2Activity.this, AdminActivity.class);
                    startActivity(novi);
                } else {
                    if(quizCount!=0) {
                        firstQuestion = parsedObject.get(currentQuizQuestion);
                        quizQuestion.setText(firstQuestion.getText());
                        One.setText(Integer.toString(firstQuestion.getAnswer()));
                    }
                    else{
                        Toast.makeText(Potvrdi2Activity.this, "Nema pitanja za potvrdjivanje", Toast.LENGTH_LONG).show();
                        Intent novi = new Intent(Potvrdi2Activity.this, AdminActivity.class);
                        startActivity(novi);
                    }
                }
            }
        });

        Potvrdi.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v){
                BackGround b = new BackGround();
                String pom= Integer.toString(firstQuestion.getId());
                b.execute(pom);

            }
        });
    }

    private class AsyncJsonObject extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        @Override
        protected String doInBackground(String... params) {
            HttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
            HttpPost httpPost = new HttpPost("http://192.168.1.11/Client/getPotvrdi2.php");
            String jsonResult = "";

            try {
                HttpResponse response = httpClient.execute(httpPost);
                jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
                System.out.println("Returned Json object " + jsonResult.toString());
            } catch (ClientProtocolException e) {
// TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
// TODO Auto-generated catch block
                e.printStackTrace();
            }
            return jsonResult;
        }
        @Override

        protected void onPreExecute() {

// TODO Auto-generated method stub

            super.onPreExecute();

            progressDialog = ProgressDialog.show(Potvrdi2Activity.this, "Downloading Questions","Wait....", true);

        }

        @Override

        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            progressDialog.dismiss();

            if(result.equals("[]")){

                Toast.makeText(Potvrdi2Activity.this, "Nema pitanja za potvrdjivanje", Toast.LENGTH_LONG).show();
                Intent novi = new Intent(Potvrdi2Activity.this, AdminActivity.class);
                startActivity(novi);
                return;
            }


            parsedObject = returnParsedJsonObject(result);

            if(parsedObject == null){

                Toast.makeText(Potvrdi2Activity.this, "Nema pitanja za potvrdjivanje", Toast.LENGTH_LONG).show();
                Intent novi = new Intent(Potvrdi2Activity.this, AdminActivity.class);
                startActivity(novi);


            }

            quizCount=parsedObject.size();
            if(quizCount==0)
            {
                Toast.makeText(Potvrdi2Activity.this, "Nema pitanja za potvrdjivanje", Toast.LENGTH_LONG).show();
                Intent novi = new Intent(Potvrdi2Activity.this, AdminActivity.class);
                startActivity(novi);
            }

            firstQuestion=parsedObject.get(0);

            quizQuestion.setText(firstQuestion.getText());

            //String[] possibleAnswers = firstQuestion.getAnswers().split(",");


            One.setText(Integer.toString(firstQuestion.getAnswer()));



        }

        private StringBuilder inputStreamToString(InputStream is) {

            String rLine = "";

            StringBuilder answer = new StringBuilder();

            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            try {

                while ((rLine = br.readLine()) != null) {

                    answer.append(rLine);

                }

            } catch (IOException e) {

// TODO Auto-generated catch block

                e.printStackTrace();

            }

            return answer;

        }

    }
    class BackGround extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            int id = Integer.parseInt(params[0]);

            String data="";
            int tmp;

            try {
                URL url = new URL("http://192.168.1.11/Client/pitanje2potvrdi.php");
                String urlParams = "id="+id;

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();
                InputStream is = httpURLConnection.getInputStream();
                while((tmp=is.read())!=-1){
                    data+= (char)tmp;
                }
                is.close();
                httpURLConnection.disconnect();

                return data;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "Exception: "+e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if(s.equals("")){
                s="Data saved successfully.";

            }
            Toast.makeText(Potvrdi2Activity.this, s, Toast.LENGTH_LONG).show();

        }
    }
    @Override
    public void onBackPressed()
    {

    }

    private List<QuizWrapper2> returnParsedJsonObject(String result){
        List<QuizWrapper2> jsonObject = new ArrayList<QuizWrapper2>();
        JSONObject resultObject = null;
        JSONArray jsonArray = null;
        QuizWrapper2 newItemObject = null;
        try {
            resultObject = new JSONObject(result);
            System.out.println("Testing the water " + resultObject.toString());
            jsonArray = resultObject.optJSONArray("questions_2");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < jsonArray.length(); i++){
            JSONObject jsonChildNode = null;

            try {
                jsonChildNode = jsonArray.getJSONObject(i);
                int id = jsonChildNode.getInt("id_question");
                String question = jsonChildNode.getString("text");
                int answerCorrect = jsonChildNode.getInt("answer");
                newItemObject = new QuizWrapper2(id, question, answerCorrect);
                jsonObject.add(newItemObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonObject;
    }
}
