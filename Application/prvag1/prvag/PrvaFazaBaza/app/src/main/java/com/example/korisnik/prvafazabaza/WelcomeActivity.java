package com.example.korisnik.prvafazabaza;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class WelcomeActivity extends AppCompatActivity {
    String name, rank;
    public static int SPLASH_TIME_OUT=4000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        name = getIntent().getStringExtra("username");
        rank = getIntent().getStringExtra("rank");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent homeIntent=new Intent(WelcomeActivity.this,IgraActivity.class);
                homeIntent.putExtra("username",name);
                homeIntent.putExtra("rank",rank);
                startActivity(homeIntent);
                finish();
            }
        },SPLASH_TIME_OUT);
    }
    @Override
    public void onBackPressed()
    {

    }
}