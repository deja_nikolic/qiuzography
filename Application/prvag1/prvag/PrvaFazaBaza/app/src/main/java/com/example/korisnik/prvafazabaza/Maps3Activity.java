package com.example.korisnik.prvafazabaza;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;
import com.kosalgeek.asynctask.AsyncResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Maps3Activity extends AppCompatActivity implements AsyncResponse,OnStreetViewPanoramaReadyCallback //implements AsyncResponse
{
    private static  LatLng SYDNEY ;
    private String NAZIV=null,ANSWER=null;
    private   Double LATITUDE=null,LONGITUDE=null;
    TextView tv;
    EditText et,et1;
   private int currentQuizQuestion;
    private int quizCount;
    private QuizWrapper4 firstQuestion;
    private List<QuizWrapper4> parsedObject;
    Integer rezultat;
    String username;
    Integer rank;
    SupportStreetViewPanoramaFragment streetViewPanoramaFragment;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps3);
        et=(EditText) findViewById(R.id.editt);
        et1=(EditText)findViewById(R.id.editt);
        String id ="1";//id pitanja
        BackGround b=new BackGround();
        b.execute(id);
        tv=(TextView)findViewById(R.id.tekst);
        currentQuizQuestion=0;
        rezultat=Integer.parseInt(getIntent().getStringExtra("rezultat"));
        username=getIntent().getStringExtra("username");
        rank=Integer.parseInt(getIntent().getStringExtra("rank"));
        //ovo treba u post
        streetViewPanoramaFragment =
                (SupportStreetViewPanoramaFragment)
                        getSupportFragmentManager().findFragmentById(R.id.streetviewpanorama);
    //    streetViewPanoramaFragment.getStreetViewPanoramaAsync(this);
      //  streetViewPanoramaFragment.on
        et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  if(et.getText().equals(firstQuestion.getAnsw())) {
                    Toast.makeText(Maps3Activity.this, "You got the answer correct", Toast.LENGTH_LONG).show();
                    rezultat += 10;
                }
                else
                    Toast.makeText(Maps3Activity.this, "You chose the wrong answer", Toast.LENGTH_LONG).show();
                currentQuizQuestion++;

                Toast.makeText(Maps3Activity.this, "Dobili ste rez: "+rezultat+" poena", Toast.LENGTH_LONG).show();
                et.setText("");
                if (currentQuizQuestion >= quizCount) {
                    Intent u1=new Intent(Maps3Activity.this,IgraActivity.class);
                    u1.putExtra("rezultat",rezultat.toString());
                    u1.putExtra("username",username);
                    u1.putExtra("rank",rank);
                    startActivity(u1);
                } else {
                    firstQuestion = parsedObject.get(currentQuizQuestion);
                    tv.setText("Lokacija?");
                    streetViewPanoramaFragment.getStreetViewPanoramaAsync(Maps3Activity.this);
                }
                return;*/
            }

        });
        et.setOnEditorActionListener(new onEditorActionListener(){
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_DONE){
                    v.performClick();
                    String s=et1.getText().toString();
                    s = s.replaceAll("\\s+","");
                    firstQuestion.setAnsw(firstQuestion.getAnsw().replaceAll("\\s+",""));
                    //s.toLowerCase();
                    firstQuestion.setAnsw(firstQuestion.getAnsw());
                    if(s.equals(firstQuestion.getAnsw())) {
                        Toast.makeText(Maps3Activity.this, "Tacan odgovor", Toast.LENGTH_LONG).show();
                        rezultat += 10;
                    }
                    else
                        Toast.makeText(Maps3Activity.this, "Pogresan odgovor.", Toast.LENGTH_LONG).show();
                    currentQuizQuestion++;

                    Toast.makeText(Maps3Activity.this, "Trenutan broj poena: "+rezultat, Toast.LENGTH_LONG).show();
                    et.setText("");
                    if (currentQuizQuestion >= quizCount) {
                        Intent u1=new Intent(Maps3Activity.this,IgraActivity.class);
                       // u1.putExtra("rezultat",rezultat.toString());
                        rank+=rezultat;
                        u1.putExtra("username",username);
                        u1.putExtra("rank",rank.toString());
                        startActivity(u1);
                    } else {
                        firstQuestion = parsedObject.get(currentQuizQuestion);
                       // tv.setText("Lokacija?");
                        tv.setText("Koji objekat je ispred vas?");
                        streetViewPanoramaFragment.getStreetViewPanoramaAsync(Maps3Activity.this);
                    }

                    return true;
                }
                return false;
            }
        });


    }

    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama panorama) {
             panorama.setPosition(new LatLng(firstQuestion.getLat(), firstQuestion.getLon()));

    }

   @Override
    public void processFinish(String s) {

    }
    @Override
    public void onBackPressed()
    {

    }


    class BackGround extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... params) {
            String id = params[0];//parametar porslednjen sa predhodne forme


            String data = "";
            int tmp;

            try {
                URL url = new URL("http://192.168.1.11/Client/getFive.php");
                String urlParams = "id=" + id ;
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while ((tmp = is.read()) != -1) {
                    data += (char) tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
        }

        @Override

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
           // progressDialog.dismiss();
            System.out.println("Resulted Value: " + result);
            parsedObject = returnParsedJsonObject(result);
            if(parsedObject == null){
                return;
            }

            quizCount=parsedObject.size();
            firstQuestion=parsedObject.get(0);
            tv.setText("Koji objekat je ispred vas?");
            streetViewPanoramaFragment.getStreetViewPanoramaAsync(Maps3Activity.this);
        }

        private StringBuilder inputStreamToString(InputStream is) {
            String rLine = "";
            StringBuilder answer = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            try {
                while ((rLine = br.readLine()) != null) {
                    answer.append(rLine);
                }
            } catch (IOException e) {
// TODO Auto-generated catch block
                e.printStackTrace();
            }
            return answer;
        }
    }
   /*    private class onEditorActionListener implements TextView.OnEditorActionListener {

           @Override
           public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
               return false;
           }
       }*/
       private List<QuizWrapper4> returnParsedJsonObject(String result){
        List<QuizWrapper4> jsonObject = new ArrayList<QuizWrapper4>();
        JSONObject resultObject = null;
        JSONArray jsonArray = null;
        QuizWrapper4 newItemObject = null;
        try {
            resultObject = new JSONObject(result);
            System.out.println("Testing the water " + resultObject.toString());
            jsonArray = resultObject.optJSONArray("questions_5");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < jsonArray.length(); i++){
            JSONObject jsonChildNode = null;

            try {
                jsonChildNode = jsonArray.getJSONObject(i);
                int id = jsonChildNode.getInt("id_question");
                //String question = jsonChildNode.getString("text");
                double lat = jsonChildNode.getDouble("latitude");
                double lon = jsonChildNode.getDouble("longitude");
                String answer = jsonChildNode.getString("answer");
                newItemObject = new QuizWrapper4(id, lat, lon,answer);
                jsonObject.add(newItemObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonObject;
    }
}
