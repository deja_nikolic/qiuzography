package com.example.korisnik.prvafazabaza;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.kosalgeek.asynctask.AsyncResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.example.korisnik.prvafazabaza.R.drawable.b;


public class MainActivity extends AppCompatActivity implements AsyncResponse,View.OnClickListener,OnConnectionFailedListener{

    EditText etUsername, etPassword;
    String Name,Password;
    String NAME=null, PASSWORD=null, EMAIL=null,RANK=null,ADMIN=null,RANK1=null;
    Button btnLogin,btnlg1;
    TextView txtaca;
    public static int SPLASH_TIME_OUT=4000;

    private SignInButton SignIn;
    private Button SingOut;
    private GoogleApiClient googleApiClient;
    private static final int REQ_CODE=9001;
    private String username,email;
    String s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etUsername = (EditText)findViewById(R.id.etUsername);
        etPassword = (EditText)findViewById(R.id.etPassword);
        btnLogin = (Button)findViewById(R.id.btnLogin);
        txtaca=(TextView)findViewById(R.id.aca);
        txtaca.setOnClickListener(this);

        SignIn=(SignInButton)findViewById(R.id.login_buttonG);
        SignIn.setOnClickListener(this);

        GoogleSignInOptions signInOptions=new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient =new GoogleApiClient.Builder(this).enableAutoManage(this,this).addApi(Auth.GOOGLE_SIGN_IN_API,signInOptions).build();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                main_login(v);

            }
        });
    }

    public void main_login(View v){
        Name = etUsername.getText().toString();
        Password = etPassword.getText().toString();
        s="SignIn";
        if(Name.equals("") || Name.equals(""))
        {
            Toast.makeText(this, "Unesite korisnicko ime i lozinku. ",
                    Toast.LENGTH_LONG).show();
        }
        else {
            BackGround b = new BackGround();
            b.execute(Name, Password, s);
        }
    }

    @Override
    public void processFinish(String output) {
        if(output.equals("success")){
            Toast.makeText(this, "Login Successfully",
                    Toast.LENGTH_LONG).show();
            Intent init =new Intent(this,WelcomeActivity.class);
            startActivity(init);

        }
        else
        {
            Toast.makeText(this, "Login Faild",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.aca:
                startActivity(new Intent(MainActivity.this,SignUp.class));
                break;
            case R.id.login_buttonG:
                signIn();
                break;

        }
    }
    private void signIn()
    {
        Intent intent=Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent,REQ_CODE);
    }
    private void handleResult(GoogleSignInResult result)
    {
        if(result.isSuccess())
        {
            Toast.makeText(this, "Login Successfully",
                    Toast.LENGTH_LONG).show();
            GoogleSignInAccount acc=result.getSignInAccount();
            username=acc.getDisplayName();
            email=acc.getEmail();
            s="SignUp";
            BackGround b = new BackGround();
            b.execute(username, email,s);

        }
        else
        {
            Toast.makeText(this, "Login Faild",
                    Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==REQ_CODE)
        {
            GoogleSignInResult result=Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleResult(result);
        }

    }

    class BackGround extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String name = params[0];
            String password = params[1];
            String pom = params[2];
            String data = "";
            String vrati="";
            int tmp;

            try {
                if (pom.equals("SignIn")) {
                    URL url = new URL("http://192.168.1.11/Client/login.php");
                    String urlParams = "txtUsername=" + name + "&txtPassword=" + password;

                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setDoOutput(true);
                    OutputStream os = httpURLConnection.getOutputStream();
                    os.write(urlParams.getBytes());
                    os.flush();
                    os.close();

                    InputStream is = httpURLConnection.getInputStream();
                    while ((tmp = is.read()) != -1) {
                        data += (char) tmp;
                    }

                    is.close();
                    httpURLConnection.disconnect();
                    vrati=data;
                    return data;
                } else if (pom.equals("SignUp")) {
                    URL url = new URL("http://192.168.1.11/Client/registerG.php");
                    String urlParams = "username=" + username + "&email=" + email;

                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setDoOutput(true);
                    OutputStream os = httpURLConnection.getOutputStream();
                    os.write(urlParams.getBytes());
                    os.flush();
                    os.close();

                    InputStream is = httpURLConnection.getInputStream();
                    while ((tmp = is.read()) != -1) {
                        data += (char) tmp;
                    }

                    is.close();
                    httpURLConnection.disconnect();
                    vrati=data;
                    return data;
                }
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
            catch (IOException e)
            {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
            return vrati;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s.equals("Records added successfully.")) {
                    s = "Data saved successfully.";
                    Intent i = new Intent(MainActivity.this, WelcomeActivity.class);
                    i.putExtra("username", username);
                    i.putExtra("rank", "0");

                startActivity(i);
            } else {
                String err = null;
                String pom = null;
                String s1=s;

               // String pom1=s1.substring(s1.indexOf("K"),s1.indexOf("P"));
                if (!s1.equals("ï»¿Korisnik ne postoji u bazi podatakaP\r\n"))
                {
                    pom = s.substring(s.indexOf('{'), s.lastIndexOf('}') + 1);
                    try {
                        JSONObject root = new JSONObject(pom);
                        JSONObject user_data = root.getJSONObject("user_data");
                        NAME = user_data.getString("name");
                        PASSWORD = user_data.getString("password");
                        EMAIL = user_data.getString("email");
                        RANK = user_data.getString("rank");
                        ADMIN = user_data.getString("admin");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        err = "Exception: " + e.getMessage();
                    }
                if (ADMIN.equals("a")) {
                    Intent i = new Intent(MainActivity.this, AdminActivity.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(MainActivity.this, WelcomeActivity.class);
                    i.putExtra("username", NAME);
                    i.putExtra("password", PASSWORD);
                    i.putExtra("email", EMAIL);
                    i.putExtra("rank", RANK);
                    i.putExtra("admin", ADMIN);
                    i.putExtra("err", err);
                    startActivity(i);
                }
            }
            else
                    Toast.makeText(MainActivity.this, "Pogresno uneti parametri ",Toast.LENGTH_LONG).show();

            }
        }
    }
}