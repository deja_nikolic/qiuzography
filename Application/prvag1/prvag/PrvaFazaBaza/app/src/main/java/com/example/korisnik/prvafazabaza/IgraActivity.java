package com.example.korisnik.prvafazabaza;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.kosalgeek.asynctask.AsyncResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class IgraActivity extends AppCompatActivity implements AsyncResponse,View.OnClickListener,GoogleApiClient.OnConnectionFailedListener {

    String username, rank;
    TextView nameTV, rankTV;
    Button igraj,predlozi,tabela,izlaz,uputstvo;
    Integer rezultat,rez;
    private GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_igra);
        rezultat=0;
        nameTV = (TextView) findViewById(R.id.user);
        rankTV = (TextView) findViewById(R.id.rank);
        igraj=(Button)findViewById(R.id.igraj);
        igraj.setOnClickListener(this);
        izlaz=(Button)findViewById(R.id.izlaz);
        izlaz.setOnClickListener(this);
        predlozi=(Button)findViewById(R.id.predlozi);
        predlozi.setOnClickListener(this);
        tabela=(Button)findViewById(R.id.tabela);
        tabela.setOnClickListener(this);
        uputstvo=(Button)findViewById(R.id.uputstvo);
        uputstvo.setOnClickListener(this);

        username = getIntent().getStringExtra("username");
        rank = getIntent().getStringExtra("rank");
       // if(getIntent().getA
       // rez=Integer.parseInt(getIntent().getStringExtra("rezultat"));
     //   rank+=rez;
        GoogleSignInOptions signInOptions=new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient =new GoogleApiClient.Builder(this).enableAutoManage(this,this).addApi(Auth.GOOGLE_SIGN_IN_API,signInOptions).build();

        nameTV.setText("Username: "+username);
        rankTV.setText("Points: "+rank);

       BackGround b = new BackGround();
        b.execute(username, rank);
    }

    @Override
    public void onBackPressed() {


    }
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.igraj:
                Intent i=new Intent(IgraActivity.this,Question1Activity.class);
                i.putExtra("rezultat", rezultat.toString());
                i.putExtra("username",username);
                i.putExtra("rank",rank);
               // i.putExtra("err", err);
                startActivity(i);
                break;
            case R.id.predlozi:
                Intent j=new Intent(IgraActivity.this,PredloziActivity.class);//ovde je izmena
                startActivity(j);
                break;
            case R.id.tabela:
                startActivity(new Intent(IgraActivity.this,TabelaActivity.class));
                break;
            case R.id.izlaz:
                izadji();
                break;
            case R.id.uputstvo:
                startActivity(new Intent(IgraActivity.this,UputstvoActivity.class));
                break;
       /* switch
                startActivity(new Intent(IgraActivity.this,Question1Activity.class));*/

        }
}

    private void izadji() {

       Intent i= new Intent(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_HOME);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);

    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<com.google.android.gms.common.api.Status>() {
            @Override
            public void onResult(@NonNull com.google.android.gms.common.api.Status status) {
                System.exit(0);
            }
        });

    }


    @Override
    public void processFinish(String s) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    class BackGround extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String name = params[0];
            String rank = params[1];
            String data = "";
            int tmp;

            try {

                //  URL url = new URL("http://10.66.148.246/Client/login.php");
                URL url = new URL("http://192.168.1.11/Client/brPoena.php");
                String urlParams = "username=" + name + "&rank=" + rank;

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();

                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while ((tmp = is.read()) != -1) {
                    data += (char) tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
        }




        @Override
        protected void onPostExecute(String s) {
            String err = null;
           // Toast.makeText(IgraActivity.this, "PHP vraca "+s, Toast.LENGTH_LONG).show();
       /*     String pom=null;
            pom=s.substring(s.indexOf('{'),s.lastIndexOf('}')+1);
            try {
                JSONObject root = new JSONObject(pom);
                JSONObject user_data = root.getJSONObject("user_data");
                NAME = user_data.getString("name");
                PASSWORD = user_data.getString("password");
                EMAIL = user_data.getString("email");
                RANK = user_data.getString("rank");
                ADMIN=user_data.getString("admin");

            } catch (JSONException e) {
                e.printStackTrace();
                err = "Exception: " + e.getMessage();
            }
            // Intent init =new Intent(MainActivity.this,WelcomeActivity.class);
            if(ADMIN.equals("a")) {
                Intent i = new Intent(MainActivity.this, AdminActivity.class);
                startActivity(i);
            }
            else {
                Intent i = new Intent(MainActivity.this, IgraActivity.class);
                i.putExtra("username", NAME);
                i.putExtra("password", PASSWORD);
                i.putExtra("email", EMAIL);
                i.putExtra("rank", RANK);
                i.putExtra("admin", ADMIN);
                i.putExtra("err", err);
                startActivity(i);
            }
            //startActivity(init);*/

        }
    }
}
