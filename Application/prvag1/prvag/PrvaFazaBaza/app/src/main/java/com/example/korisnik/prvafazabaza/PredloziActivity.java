package com.example.korisnik.prvafazabaza;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PredloziActivity extends AppCompatActivity implements View.OnClickListener {

    Button p1,p2,p3,p4,p5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_predlozi);
        p1=(Button)findViewById(R.id.Pitanje1);
        p1.setOnClickListener(this);
        p2=(Button)findViewById(R.id.Pitanje2);
        p2.setOnClickListener(this);
     //   p3=(Button)findViewById(R.id.Pitanje3);
     //   p3.setOnClickListener(this);
        p4=(Button)findViewById(R.id.Pitanje4);
        p4.setOnClickListener(this);
        p5=(Button)findViewById(R.id.Pitanje5);
        p5.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.Pitanje1:
                startActivity(new Intent(PredloziActivity.this,Predlog1Activity.class));
                break;
            case R.id.Pitanje2:
                startActivity(new Intent(PredloziActivity.this,Predlog2Activity.class));
                break;
       /*     case R.id.Pitanje3:
                startActivity(new Intent(PredloziActivity.this,Predlog3Activity.class));
                break;*/
            case R.id.Pitanje4:
                startActivity(new Intent(PredloziActivity.this,Predlog4Activity.class));
                break;
            case R.id.Pitanje5:
                startActivity(new Intent(PredloziActivity.this,Predlog5Activity.class));
                break;


        }
    }

}
