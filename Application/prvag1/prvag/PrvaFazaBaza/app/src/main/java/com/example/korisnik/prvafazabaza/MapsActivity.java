package com.example.korisnik.prvafazabaza;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kosalgeek.asynctask.AsyncResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements AsyncResponse,OnMapReadyCallback ,GoogleMap.OnMapClickListener {

    private GoogleMap mMap;
    private LatLng kordinate;
    private String NAZIV=null;
    private Integer LATITUDE=null,LONGITUDE=null;
    private int currentQuizQuestion;
    private int quizCount;
    private QuizWrapper4 firstQuestion;
    private List<QuizWrapper4> parsedObject;
    Integer rezultat;
    String username,rank;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        rezultat=Integer.parseInt(getIntent().getStringExtra("rezultat"));
        username=getIntent().getStringExtra("username");
        rank=getIntent().getStringExtra("rank");
       // Question2Activity.AsyncJsonObject asyncObject = new Question2Activity.AsyncJsonObject();
        String id ="1";//id pitanja
        BackGround b=new BackGround();
        b.execute(id);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.getUiSettings().setZoomGesturesEnabled(true);
         mMap.clear();
       // mMap.clear();
       // mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        LatLng sydney = new LatLng(43.3209, 21.8958);
        MapStyleOptions style=MapStyleOptions.loadRawResourceStyle(this,R.raw.style_json);
        mMap.setMapStyle(style);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Nis"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.setOnMapClickListener(this);
     //   mMap
        // Add a marker in Sydney and move the camera

    }
    @Override
    public void onBackPressed()
    {

    }

    @Override
    public void onMapClick(LatLng point) {
  /*  kordinate=point;
        Toast.makeText(this, "kordinate:"+point.latitude+" "+point.longitude,
                Toast.LENGTH_LONG).show();*/
        currentQuizQuestion++;
        double dist=Math.sqrt((point.latitude-firstQuestion.getLat())*(point.latitude-firstQuestion.getLat())+(point.longitude- firstQuestion.getLon())*(point.longitude-firstQuestion.getLon()));
       // Toast.makeText(this, "kordinate:"+dist, Toast.LENGTH_LONG).show();
     //   dist=dist*10;
        dist=10-(dist*5);
        if(dist<0)
            dist=0;
        int vred=(int) dist ;
      //  Toast.makeText(this, "Dobili ste:"+vred+" poena", Toast.LENGTH_LONG).show();
        rezultat+=vred;
        Toast.makeText(this, "Trenutan broj poena: "+rezultat, Toast.LENGTH_LONG).show();
        if (currentQuizQuestion >= quizCount) {
            Intent u1=new Intent(MapsActivity.this,Maps3Activity.class);
               // u1.putExtra("rezultat",rezultat.toString());
            Integer pom=Integer.parseInt(rank);//+rezultat;
            u1.putExtra("rank",pom.toString());
            u1.putExtra("username",username);
            u1.putExtra("rezultat",rezultat.toString());
            startActivity(u1);
        } else {
            firstQuestion = parsedObject.get(currentQuizQuestion);
            Toast.makeText(MapsActivity.this,"Pronadjite :"+ firstQuestion.getText(), Toast.LENGTH_LONG).show();
        }
        return;

    }
    @Override
    public void processFinish(String s) {

    }
    class BackGround extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... params) {
            String id = params[0];//parametar porslednjen sa predhodne forme


            String data = "";
            int tmp;

            try {
                URL url = new URL("http://192.168.1.11/Client/getFour.php");
                String urlParams = "id=" + id ;
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                while ((tmp = is.read()) != -1) {
                    data += (char) tmp;
                }

                is.close();
                httpURLConnection.disconnect();

                return data;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
        }
        @Override
        protected void onPostExecute(String result) {
       /*     String err = null;
            String pom=null;
      //      pom=s.substring(s.indexOf('{'),s.lastIndexOf('}')+1);
            try {
                JSONObject root = new JSONObject(s);
                JSONObject user_data = root.getJSONObject("user_data");
                NAZIV = user_data.getString("naziv");
                LATITUDE = user_data.getInt("longitude");
                LONGITUDE = user_data.getInt("latitude");
              //  String s=s.toString(LATITUDE);
                Intent init =new Intent(MapsActivity.this,WelcomeActivity.class);
            //    Toast.makeText(this, "kordinate:"+LATITUDE+" "+LONGITUDE,
              //          Toast.LENGTH_LONG).show();
                Toast.makeText(MapsActivity.this,"Pronadjite :"+ NAZIV, Toast.LENGTH_LONG).show();
            } catch (JSONException e) {
                e.printStackTrace();
                err = "Exception: " + e.getMessage();
            }

            // Intent init =new Intent(MainActivity.this,WelcomeActivity.class);
       /*     if(ADMIN.equals("a")) {
                Intent i = new Intent(MainActivity.this, AdminActivity.class);
                startActivity(i);
            }
            else {
                Intent i = new Intent(MainActivity.this, IgraActivity.class);
                i.putExtra("name", NAME);
                i.putExtra("password", PASSWORD);
                i.putExtra("email", EMAIL);
                i.putExtra("rank", RANK);
                i.putExtra("admin", ADMIN);
                i.putExtra("err", err);
                startActivity(i);
            }
            //startActivity(init);*/
            super.onPostExecute(result);
           // progressDialog.dismiss();
            System.out.println("Resulted Value: " + result);
            parsedObject = returnParsedJsonObject(result);
            if(parsedObject == null){
                return;
            }

            quizCount=parsedObject.size();
            firstQuestion=parsedObject.get(0);
         //   question4.setText(firstQuestion.getText());
            Toast.makeText(MapsActivity.this,"Pronadjite :"+ firstQuestion.getText(), Toast.LENGTH_LONG).show();
        }

        private StringBuilder inputStreamToString(InputStream is) {
            String rLine = "";
            StringBuilder answer = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            try {
                while ((rLine = br.readLine()) != null) {
                    answer.append(rLine);
                }
            } catch (IOException e) {
// TODO Auto-generated catch block
                e.printStackTrace();
            }
            return answer;
        }
        private List<QuizWrapper4> returnParsedJsonObject(String result){
            List<QuizWrapper4> jsonObject = new ArrayList<QuizWrapper4>();
            JSONObject resultObject = null;
            JSONArray jsonArray = null;
            QuizWrapper4 newItemObject = null;
            try {
                resultObject = new JSONObject(result);
                System.out.println("Testing the water " + resultObject.toString());
                jsonArray = resultObject.optJSONArray("questions_4");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            for(int i = 0; i < jsonArray.length(); i++){
                JSONObject jsonChildNode = null;

                try {
                    jsonChildNode = jsonArray.getJSONObject(i);
                    int id = jsonChildNode.getInt("id_question");
                    String question = jsonChildNode.getString("NazivLokacije");
                    //int answerCorrect = jsonChildNode.getInt("answer");
                    double lat=jsonChildNode.getDouble("latitude");
                    double lon=jsonChildNode.getDouble("longitude");
                    newItemObject = new QuizWrapper4(id, question, lat,lon);
                    jsonObject.add(newItemObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return jsonObject;
        }
    }
}
