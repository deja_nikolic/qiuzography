package com.example.korisnik.prvafazabaza;


import android.media.Image;
import android.net.Uri;

public class QuizWrapper3 {
    private int id_question;

    private String  text;

    private String image;

    private String answer;


    public QuizWrapper3(String image){
        this.image=image;
    }


    public QuizWrapper3(int id_question, String text, String image, String answer) {

        this.id_question = id_question;
        this.text=text;
        this.image = image;
        this.answer=answer;

    }




    public int getId() {

        return id_question;

    }

    public void setId(int id) {

        this.id_question = id_question;

    }

    public String getText() {

        return text;

    }

    public void setText(String text) {

        this.text = text;

    }

    public String getImage() {

        return image;

    }

    public void setImage(String image) {

        this.image = image;

    }

    public String getAnswer() {

        return answer;

    }

    public void setAnswer(String get2) {

        this.answer = answer;

    }


}
