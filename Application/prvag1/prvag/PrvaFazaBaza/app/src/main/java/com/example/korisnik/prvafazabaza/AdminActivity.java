package com.example.korisnik.prvafazabaza;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AdminActivity extends AppCompatActivity implements View.OnClickListener {

    Button p1,p2,p4,p5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        p1=(Button)findViewById(R.id.P1);
        p1.setOnClickListener(this);
        p2=(Button)findViewById(R.id.P2);
        p2.setOnClickListener(this);

        p4=(Button)findViewById(R.id.P4);
        p4.setOnClickListener(this);
        p5=(Button)findViewById(R.id.P5);
        p5.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.P1:
                startActivity(new Intent(AdminActivity.this,Potvrdi1Activity.class));
                break;
            case R.id.P2:
                startActivity(new Intent(AdminActivity.this,Potvrdi2Activity.class));
                break;
            case R.id.P4:
                startActivity(new Intent(AdminActivity.this,Potvrdi4Activity.class));
                break;
            case R.id.P5:
                startActivity(new Intent(AdminActivity.this,Potvrdi5Activity.class));
                break;


        }
    }
}
