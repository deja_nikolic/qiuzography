package com.example.korisnik.prvafazabaza;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Question2Activity extends AppCompatActivity  {
    private TextView question2;
    private Button submit;
    private EditText answer2;
    private int currentQuizQuestion;
    private int quizCount;
    private QuizWrapper2 firstQuestion;
    private List<QuizWrapper2> parsedObject;
    int vred;
    Integer rezultat;
    String rank,username;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question2);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        question2 = (TextView) findViewById(R.id.qst2);
        vred=0;
        rezultat=Integer.parseInt(getIntent().getStringExtra("rezultat"));
        username=getIntent().getStringExtra("username");
        rank=getIntent().getStringExtra("rank");
       // submit = (Button) findViewById(R.id.btnSubmit);
        answer2 = (EditText) findViewById(R.id.ans);
        Button nextButton = (Button) findViewById(R.id.btnNext);
        Question2Activity.AsyncJsonObject asyncObject = new Question2Activity.AsyncJsonObject();

            asyncObject.execute("");

        EditText et=(EditText)findViewById(R.id.ans);
        et.setOnEditorActionListener(new onEditorActionListener(){

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_DONE){
                    // submit.performClick();
                    InputMethodManager imm=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(answer2.getWindowToken(),0);
                    int check = Integer.parseInt(answer2.getText().toString());
                    if(firstQuestion.getAnswer()<check) {
                        double d = (double) firstQuestion.getAnswer() / check;
                        d -= 1 - d;
                        d = d * 10;
                         vred = (int) d;
                    }
                    else
                    {
                        double d = (double) firstQuestion.getAnswer() / check;
                        d -= 2*(d-1);
                        d = d * 10;
                         vred = (int) d;
                    }
                    if(vred<0)
                        vred=0;
                    //inkrement broj poena
                        rezultat+=vred;

                    return true;
                }
                return false;
            }
        });


        nextButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                currentQuizQuestion++;

                Toast.makeText(Question2Activity.this, "Osvoili ste: "+rezultat+" poena", Toast.LENGTH_LONG).show();
                answer2.setText("");
                if (currentQuizQuestion >= quizCount) {
                    Intent u1=new Intent(Question2Activity.this,MapsActivity.class);
                    u1.putExtra("rezultat",rezultat.toString());
                    u1.putExtra("username",username);
                    u1.putExtra("rank",rank);
                    startActivity(u1);
                } else {
                    firstQuestion = parsedObject.get(currentQuizQuestion);
                    question2.setText(firstQuestion.getText());

                }
                return;
            }
        });

        final View.OnClickListener myHandler = new View.OnClickListener() {
            public void onClick(View v) {
            }

        };
//dodaj za multiplay!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
   /*     submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int check=Integer.parseInt(answer2.getText().toString());
                if(check==firstQuestion.getAnswer()){
                    Toast.makeText(Question2Activity.this, "You got the answer correct", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(Question2Activity.this, "You chose the wrong answer", Toast.LENGTH_LONG).show();
                }
            }

        });*/

    }

    //private
    @Override
    public void onBackPressed()
    {

    }
    private class AsyncJsonObject extends AsyncTask<String, Void, String> {
        private ProgressDialog progressDialog;
        @Override
        protected String doInBackground(String... params) {
            HttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
            HttpPost httpPost = new HttpPost("http://192.168.1.11/Client/getTwo.php");
            String jsonResult = "";

            try {
                HttpResponse response = httpClient.execute(httpPost);
                jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
                System.out.println("Returned Json object " + jsonResult.toString());
            } catch (ClientProtocolException e) {
// TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
// TODO Auto-generated catch block
                e.printStackTrace();
            }
            return jsonResult;
        }


        @Override

        protected void onPreExecute() {
// TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = ProgressDialog.show(Question2Activity.this, "Downloading Quiz","Wait....", true);
        }

        @Override

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            System.out.println("Resulted Value: " + result);
            parsedObject = returnParsedJsonObject(result);
            if(parsedObject == null){
                return;
            }

            quizCount=parsedObject.size();
            firstQuestion=parsedObject.get(0);
            question2.setText(firstQuestion.getText());

        }

        private StringBuilder inputStreamToString(InputStream is) {
            String rLine = "";
            StringBuilder answer = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            try {
                while ((rLine = br.readLine()) != null) {
                    answer.append(rLine);
                }
            } catch (IOException e) {
// TODO Auto-generated catch block
                e.printStackTrace();
            }
            return answer;
        }
    }
 /*   private class onEditorActionListener implements TextView.OnEditorActionListener {

    }*/
    private List<QuizWrapper2> returnParsedJsonObject(String result){
        List<QuizWrapper2> jsonObject = new ArrayList<QuizWrapper2>();
        JSONObject resultObject = null;
        JSONArray jsonArray = null;
        QuizWrapper2 newItemObject = null;
        try {
            resultObject = new JSONObject(result);
            System.out.println("Testing the water " + resultObject.toString());
            jsonArray = resultObject.optJSONArray("questions_2");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < jsonArray.length(); i++){
            JSONObject jsonChildNode = null;

            try {
                jsonChildNode = jsonArray.getJSONObject(i);
                int id = jsonChildNode.getInt("id_question");
                String question = jsonChildNode.getString("text");
                int answerCorrect = jsonChildNode.getInt("answer");
                newItemObject = new QuizWrapper2(id, question, answerCorrect);
                jsonObject.add(newItemObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonObject;
    }


}
