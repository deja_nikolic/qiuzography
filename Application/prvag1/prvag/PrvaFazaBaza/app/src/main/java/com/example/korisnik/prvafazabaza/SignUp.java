package com.example.korisnik.prvafazabaza;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class SignUp extends AppCompatActivity implements View.OnClickListener {
    EditText username, password, email;//,lastname;
    Button signUp;
    String Username,Email, Password;// Lastname;
    Context ctx=this;
    String NAME=null,RANK="0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        email = (EditText) findViewById(R.id.email);
        signUp=(Button)findViewById(R.id.signup1);
        signUp.setOnClickListener( this);
       // lastname = (EditText) findViewById(R.id.lastname);

            }
    public void onClick(View v) {
    register_register(v);

    }
    public void register_register(View v){
        Username = username.getText().toString();
        Password = password.getText().toString();
     //   Name = name.getText().toString();
        Email = email.getText().toString();
        if(Username.equals("")|| Password.equals("")||Email.equals(""))
        {
            Toast.makeText(ctx, "Nepravilno uneti parametri.", Toast.LENGTH_LONG).show();
        }
        else {
            BackGround b = new BackGround();
            b.execute(Username, Password, Email);
        }
    }
    @Override
    public void onBackPressed()
    {

    }
    class BackGround extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String username = params[0];
            String password = params[1];
            String email = params[2];

            String data = "";
            int tmp;

            try {
                URL url = new URL("http://192.168.1.11/Client/register.php");
                String urlParams = "username=" + username + "&password=" + password + "&email=" + email;

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                os.write(urlParams.getBytes());
                os.flush();
                os.close();
                InputStream is = httpURLConnection.getInputStream();
                while ((tmp = is.read()) != -1) {
                    data += (char) tmp;
                }
                is.close();
                httpURLConnection.disconnect();

                return data;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
        }


        @Override
        protected void onPostExecute(String s) {
            if(s.equals("Records added successfully.")){
                Toast.makeText(ctx, s, Toast.LENGTH_LONG).show();
                Intent novi=new Intent(SignUp.this,WelcomeActivity.class);
                novi.putExtra("username",Username);
                novi.putExtra("rank",RANK);
                startActivity(novi);

            }



        }
    }


}
